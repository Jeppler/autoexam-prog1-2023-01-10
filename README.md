# Övningstenta – Programmerignsteknik I (2022-01-10)
## Kom igång
Öppna [huvudhemsidan](https://jeppler.gitlab.io/autoexam-prog1-2022-01-10) som har både instruktioner och filer att ladda ner. 

## Dokumentation
Öppna [dokumentationssidorna](docs/README.md) för att läsa mer om hur projektet fungerar. 
