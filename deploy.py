"""
Copyright (c) Jesper Carlsson 2023
"""

import datetime
import zipfile
import shutil
import os
import pytz

DATE = datetime.datetime.now(pytz.timezone('Europe/Stockholm')).strftime('%Y-%m-%d %H:%M')
ZIPFILE_NAME = 'public/autoexam-prog1-2022-01-10.zip'

def get_files(path):
    if len(path) == 0 or path[-1] != '/':
        path += '/'

    for filename in os.listdir(path):
        if os.path.isfile(path+filename):
            yield path+filename

# Copy folder for GitLab Pages
shutil.copytree('deploy', 'public')

# Write version number to program
with open('public_html/index.html', 'r', encoding='utf-8') as file:
    document = file.read()

with open('public_html/index.html', 'w', encoding='utf-8') as file:
    file.write(document.replace('{date}', DATE))

# Write program to zip file
with zipfile.ZipFile(ZIPFILE_NAME, 'w') as zfile:
    zfile.write('run.py')
    zfile.write('LICENSE')
    [zfile.write(filename) for filename in get_files('tests')]
    [zfile.write(filename) for filename in get_files('tests/cases')]
    [zfile.write(filename) for filename in get_files('public_html')]

# Restore source files
with open('public_html/index.html', 'w', encoding='utf-8') as file:
    file.write(document)

# Write version number to GitLab Pages
with open('public/index.html', encoding='utf-8') as zfile: 
    document = zfile.read()

with open('public/index.html', 'w', encoding='utf-8') as zfile: 
    zfile.write(document.replace('{datum}', DATE))