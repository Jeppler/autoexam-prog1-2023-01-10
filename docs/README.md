# Documentation
Det här dokumentet innehåller en översikt över projektet. Se även listan nedan på andra dokument. 

## Other Documents
 - [Testing](testing.md)
 - [Web Design](webdesign.md)

## Table of Contents
[TOC]

## File tree
```
autoexam-prog1-2023-01-10
+deploy*
|-index.html
|-style.css
+docs*
+public_html
|-favicon.ico
|-index.html
|-main.js
|-style.css
|-web.py
+tests
|+cases
||-candidate.py
||-test_[...].py
|-candidate_wrapper_class.py
|-candidate_wrapper_func.py
|-main.py
|-preamble_controller.py
|-preamble.py
|-redact.py
|-timeout.py
-deploy.py*
-run.py
```

Files and directories marked ``*``, are not included in the deployed zipfile. 

### Directory: deploy
[Open directory](/deploy/)

This directory contains html/css files that make the GitLab Pages site. The ``deploy.py`` file will copy those files into the gitlab pages public folder, generate a zip file with the program and mark it with the version number. The ``deploy.py`` file, is run by the CI/CD pipline on the main branch. 

### Directory: docs
[Open directory](/docs/)

This directory contains the documentation you are reading now. 

### Directory: public_html
[Open directory](/public_html/)

This directory contains all the html/css/js files that make the web application as well as the [python webserver](/public_html/web.py). Read more [here](webdesign.md). 

### Directory: tests
[Open directory](/tests/)

This directory contains all the code, relating to testing python code. The [subdirectory cases](/tests/cases/) contains individual testcases as well as the candidate file. Read more [here](testing.md). 

### Python file: run
[Open file](/run.py)

This python file, starts the entire application. 