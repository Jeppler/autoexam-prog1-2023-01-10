[Back to overview](README.md)

# Testing
This document conatins information about how the code is tested. 

## Table of Contents
[TOC]

## Testing Overview
A test is sent from the web page as a [POST-request](webdesign.md#post-requests) to [web.py](/public_html/web.py) which in turn calls the function [``start_test()``](#start_test) in [main.py](/tests/main.py). [``start_test()``](#start_test) then writes the Python code to be tested, to the [candidate file](/tests/cases/candidate.py). Finally, it runs the test logic by calling the function ``run()`` in the appropriate test file. 

### ``start_test()``
The funtcion ``start_test(test_name: str, candidate_data: str) -> dict: `` takes two paramters, the test name and the python code a string. It returns a dictonaries with boolean values for ``tested`` and ``correct``. 

## Test files
Test files are unique for each assignment and contains the testing logic. All testfiles must contain a function ``run()`` the invokes the test and returns a ``pytest.EXIT_CODE``. 

### Candidate Wrappers
As the candidate solutions, generally consists of "loose code", we need to contain it in a wrapper. If the code should be run directly, [``candidate_wrapper_func.py``](/tests/candidate_wrapper_func.py) is used. If the code contains a function definition, [``candidate_wrapper_class.py``](/tests/candidate_wrapper_class.py) is used. This is because loose code is contained in a wrapper function and loose functions are contained in a wrapper class. 

To use the function wrapper, import the function and call it. 
```python
from tests.candidate_wrapper_func import func
import pytest

def test_runnable():
    func()

def run():
    return pytest.main(['tests/cases/test_example.py'])
```

To use the class wrapper, import the wrapper module and call the function the the candiate file. Note that the module has to be reloaded on each attempt. 

```python
import tests.candidate_wrapper_class
import pytest
import importlib

def test_runable():
    result = tests.candidate_wrapper_class.module.your_function_name()

def run():
    importlib.reload(tests.candidate_wrapper_class)
    return pytest.main(['tests/cases/test_b1.py'])
```

### Preambles
Some test cases, require some code to be run before the candidate code. For example, defining a varible or function that should be accessable to the candidate code. This is handled by [``preamble_controller.py``](/tests/preable_controller.py). 

When [``main.py``](/tests/main.py) writes the candidate file, it prepends ``from tests.preamble import *`` to the candidate file. [``preamble_controller.py``](/tests/preable_controller.py) defines a class ``Preabmle``. It should be used in a context manager and will write the preamble to [``preamble.py``](/tests/preamble.py) before the test and removes it after. 

### Timeout
To avoid hanging the program if the candidate code contains an infinte loop or other slow algoritm, a maximum execusion time of 250 ms is set for each test. [``timeout.py``](/tests/timeout.py) defines a decorator function that runs the assoiated function in a diffrent thread and raises ``TimeoutError`` the maximum execution time is exceeded. 

The decorator function is used in the [wrapper files](#candidate-wrappers).

### Redact Varibles and Patterns
Sometimes, the candidate might contain a varible defintion that you want to overwrite in a test to vary the test data. The [``redact.py``](/tests/redact.py) offers two functions for this, ``redact_varible()`` and ``redact_pattern()``. 

``redact_varible()`` will remove all assignments to the varibles named as arguments. This function is easy to use but will remove all assignments, not just the inital one. 

``redact_pattern()`` will redact everything matching the supplied regex pattern. This can be used if you want to remove the inital defintion of a varible but want to allow reassignments. 

Redact functions should be called in the ``run()`` function. 

### CaptureFixture
CaptureFixture from pytest is used to capture data printed from the candidate files. 

### MonkeyPatch
MonkeyPatch from pytest is used to overwrite existing function defintions. This can for example be used to overwrite ``input()``  or ``random.randint()`` to make it return predefined data. 
