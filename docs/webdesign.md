[Back to overview](README.md)

# Web Design
This document, contains information about the web application. 

## Table of Contents
[TOC]

## Python Web Server
The python file [``web.py``](/public_html/web.py) is the first file called, when the application is launched. It starts a webserver on [localhost:8080](http://localhost:8080). The webserver listes to GET- and POST-requests. 

### GET Requests
GET-requests are used to retrive files from the [``public_html``](/public_html/) folder. 

### POST Requests
A POST-request to ``/exit`` will close the application. 

A POST-requet to ``/test`` will start a test. This requries field data ``question`` which is the name of the question/test and ``answer`` which is a string with the python code that should be tested. ``answer`` will be written to the [candiate file](/tests/cases/candidate.py) and the test is [invoked](testing.md#start_test). 

## HTML/CSS Struture
The webpage consits of content-boxes ``<div class="content-box">...</div>``. Forms are used for questions. 

The forms have one of the following classes. 
 - ``local-test`` are used to mark normal multiple choise questions. The correct answer should have the class ``correct-answer``. 
 - ``local-test-textfield`` are used to mark textfield questions. Each input-field should have the attribute ``answer`` with the correct answer. 
 - ``local-test-table`` is used to mark tables with radio buttons. Each correct answer should have ``value="correct"``. 
 - ``remote-test`` is used to mark python code with custom [testing logic](testing.md). The form should have an id with the test-modules name (but with ``-`` instead of ``_``). 

Solutions are contained within a solution-wrapper. The solution wrapper conatins a hideable div and a show/hide button. 

## JavaScript Logic
The javascript code uses JQuery. It is used to test the local tests and send a AJAX/POST-request with the test data for remote tests. The javascript code is also used to show/hide solutions and close the application. 
