$(() => {
     $('form.remote-test').on('submit', (e) => {
        e.preventDefault();
        var answer = $('#'+ e.target.id + '>textarea').val();

        $.ajax({
            url: '/test',
            type: 'POST',
            processData: false,
            contentType: 'text/plain',
            data: JSON.stringify({question: e.target.id, answer: answer}),
            success: (test) => {
                if(test.tested !== true) {
                    $(e.target).removeClass('ok');
                    $(e.target).addClass('warning');
                    $(e.target).removeClass('error');
                    $(e.target).children('p.status').text('Kunde inte testa koden. ');
                } else if(test.correct === true) {
                    $(e.target).addClass('ok');
                    $(e.target).removeClass('warning');
                    $(e.target).removeClass('error');
                    $(e.target).children('p.status').text('Korrekt! ');
                } else {
                    $(e.target).removeClass('ok');
                    $(e.target).removeClass('warning');
                    $(e.target).addClass('error');
                    $(e.target).children('p.status').text('test');
                    $(e.target).children('p.status').text('Hmm, det är något som inte riktigt stämmer. ');
                }
            },
            error: (test) => {
                $(e.target).removeClass('ok');
                $(e.target).addClass('warning');
                $(e.target).removeClass('error');
                $(e.target).children('p.status').text('Kunde inte ansluta till servern. Kontrollera att pythonprogrammet fortfarande kör i bakgrunden. ');
            }
        });
     });

     $('form.exit').on('submit', (e) => {
        e.preventDefault();
        $('#outer-container').html('<div class="content-box"><h1>Exiting...</h1></div>');

        $.ajax({
            url: '/exit',
            type: 'POST'
        });

        setTimeout(window.close, 1000);
     });

     $('form.local-test').on('submit', (e) => {
        e.preventDefault();

        var correct = true;

        $.each($(e.target).children('label'), (_, alt) => {
            const answer = $(alt).children('input[type=checkbox]')[0];
            if(answer.checked !== $(answer).hasClass('correct-answer')) correct = false;
        });

        if(correct) {
            $(e.target).addClass('ok');
            $(e.target).removeClass('error');
            $(e.target).children('p.status').text('Korrekt! ');
        } else {
            $(e.target).removeClass('ok');
            $(e.target).addClass('error');
            $(e.target).children('p.status').text('Inte helt rätt. ');
        }
     });

     $('form.local-test-table').on('submit', (e) => {
        e.preventDefault();

        var correct = true;
        
        $.each($(e.target).find('tr:has(td)'), (_, row) => {
            if($(row).find('td input:checked').val() != 'correct') correct = false;
        });

        if(correct) {
            $(e.target).addClass('ok');
            $(e.target).removeClass('error');
            $(e.target).children('p.status').text('Korrekt! ');
        } else {
            $(e.target).removeClass('ok');
            $(e.target).addClass('error');
            $(e.target).children('p.status').text('Inte helt rätt. ');
        }
     });

     $('form.local-test-textfield').on('submit', (e) => {
        e.preventDefault()

        var correct = true;

        $.each($(e.target).find('input'), (_, question) => {
            if($(question).val().trim() != $(question).attr('answer')) correct = false;
        });

        if(correct) {
            $(e.target).addClass('ok');
            $(e.target).removeClass('error');
            $(e.target).children('p.status').text('Korrekt! ');
        } else {
            $(e.target).removeClass('ok');
            $(e.target).addClass('error');
            $(e.target).children('p.status').text('Inte helt rätt. ');
        }
     });

     $('textarea').on('keydown', (e) => {
        $(e.target).parent().removeClass('ok');
        $(e.target).parent().removeClass('warning');
        $(e.target).parent().removeClass('error');
     });

     $('div.solution-wrapper button').on('click', (e) => {
        $(e.target).parent().children('div.solution').toggle();
        if($(e.target).text() === 'Dölj lösningsförslag') $(e.target).text('Visa lösningsförslag')
        else $(e.target).text('Dölj lösningsförslag')
     });

     $.each($('code.python'), (_, code) => {
        const KEYWORDS = ['and', 'as', 'assert', 'async', 'await', 'break', 'class', 'continue', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield'];

        var text = $(code).text();
        text.replaceAll('"', '&quot;');

        text = text.replaceAll(/\w+/gmu, (match) => {
            $.each(KEYWORDS, (_, keyword) => {if(match === keyword) match = '<span class="keyword">' + match + '</span>';});
            
            if(match === 'True' || match === 'False' || isNumeric(match)) match = '<span class="value">' + match + '</span>';
            
            if(match === 'def') match = '<span class="def">' + match + '</span>';

            return match
        });

        text = text.replaceAll(/#.*/gmu, (str) => {return '<span class="comment">' + str + '</span>';});
        text = text.replaceAll(/'[^']*'/gmu, (str) => {return '<span class="string">' + str + '</span>';});
        text = text.replaceAll(/&quot;.[^']*&quot;/gmu, (str) => {return '<span class="string">' + str.replaceAll(/<span class=".*">/gmu, '').replaceAll(/<\/span>/gmu, '') + '</span>';});

        $(code).html(text)
     });
});

function isNumeric(str) {
     if(typeof str != 'string') return false;

     return !isNaN(str) && !isNaN(parseFloat(str))
}