"""
Copyright (c) Jesper Carlsson 2023
"""

from http.server import BaseHTTPRequestHandler, HTTPServer
from tests.main import start_test
import json
import webbrowser

class WebServer(BaseHTTPRequestHandler):
    READABLE_FILES = ['/', '/style.css', '/main.js', '/favicon.ico']

    TYPES = {
        'html': 'text/html',
        'css': 'text/css',
        'js': 'application/javascript',
        'ico': 'image/x-image'
    }

    def do_GET(self):
        if self.path in WebServer.READABLE_FILES:
            path = '/index.html' if self.path == '/' else self.path
            ext = path.split('.')[-1]

            if ext in WebServer.TYPES:
                self.send_response(200)
                self.send_header('Content-Type', WebServer.TYPES[ext])
            else:
                self.send_response_only(500)
                return

            self.end_headers()

            with open(f'public_html{path}', 'rb') as file:
                self.wfile.write(file.read())
        else:
            self.send_response_only(404)
    
    def do_POST(self):
        if self.path == '/exit':
            print('>>> EXITING <<<')
            exit()
        elif self.path != '/test':
            self.send_response(404)
            self.end_headers()
        
        post_data = json.loads(self.rfile.read(int(self.headers['Content-Length'])))
        
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        
        self.wfile.write(bytes(json.dumps(start_test(
            post_data['question'].replace("-", "_"),
            post_data['answer']
        )), 'utf-8'))

def run():
    webServer = HTTPServer(('localhost', 8080), WebServer)
    print('Webserver started')

    webbrowser.open('http://localhost:8080')

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print('Server stopped')