"""
Copyright (c) Jesper Carlsson 2023
"""

import public_html.web

def main():
    public_html.web.run()

if __name__ == '__main__':
    main()