"""
Copyright (c) Jesper Carlsson 2023
"""

import types
import importlib
from tests.timeout import timeout

loader = importlib.machinery.SourceFileLoader('candidate', 'tests/cases/candidate.py')
module = types.ModuleType(loader.name)
loader.exec_module(module)

for entity in dir(module):
    if type(getattr(module, entity)) == type(lambda: None):
        setattr(module, entity, timeout(.25)(getattr(module, entity)))
    
