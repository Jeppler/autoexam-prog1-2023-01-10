"""
Copyright (c) Jesper Carlsson 2023
"""

import types
import importlib
from tests.timeout import timeout

@timeout(.25)
def func():
    loader = importlib.machinery.SourceFileLoader('candidate', 'tests/cases/candidate.py')
    module = types.ModuleType(loader.name)
    loader.exec_module(module)