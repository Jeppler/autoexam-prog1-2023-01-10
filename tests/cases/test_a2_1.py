"""
Copyright (c) Jesper Carlsson 2023
"""

import pytest
from tests.candidate_wrapper_func import func
from pytest import MonkeyPatch, CaptureFixture

def test_a2_1_runnable():
    func()

def test_a2_1_check_rand_arg(monkeypatch: MonkeyPatch):
    def random(lower, higher): 
        assert lower == 1
        assert higher == 6

        return 1

    monkeypatch.setattr('random.randint', random)
    func()

def test_a2_1_correct1(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    values = [2, 1, 3, 5, 2, 5, 1, 5, 5, 6, 1, 1, 4, 4, 1, 3, 6, 4, 5, 4]

    monkeypatch.setattr('random.randint', lambda _l, _h: values.pop(0))
    func()

    out, _ = capfd.readouterr()
    assert out.split('\n')[-2] == "Number of equal elements at the same index: 2"

def test_a2_1_correct2(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    values = [2, 4, 1, 1, 2, 6, 6, 1, 1, 6, 2, 4, 2, 5, 3, 6, 3, 6, 6, 2]

    monkeypatch.setattr('random.randint', lambda _l, _h: values.pop(0))
    func()

    out, _ = capfd.readouterr()
    assert out.split('\n')[-2] == "Number of equal elements at the same index: 3"

def test_a2_1_correct3(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    values = [3, 6, 3, 1, 4, 5, 1, 4, 4, 1, 4, 1, 3, 2, 5, 5, 5, 4, 5, 2]

    monkeypatch.setattr('random.randint', lambda _l, _h: values.pop(0))
    func()

    out, _ = capfd.readouterr()
    assert out.split('\n')[-2] == "Number of equal elements at the same index: 3"

def test_a2_1_correct4(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    values = [(i % 6)+1 for i in range(10)] * 2

    monkeypatch.setattr('random.randint', lambda _l, _h: values.pop(0))
    func()

    out, _ = capfd.readouterr()
    assert out.split('\n')[-2] == "Number of equal elements at the same index: 10"

def run():
    return pytest.main(['tests/cases/test_a2_1.py'])