"""
Copyright (c) Jesper Carlsson 2023
"""

from tests.candidate_wrapper_func import func
from tests.preable_controller import Preamble
import pytest

def pollax_runnable(x, y, z):
        a = [e*x for e in z if e > y]
        return a

def test_a2_2_runnable():
    with Preamble('from tests.cases.test_a2_2 import pollax_runnable as pollax'):
        func()


def pollax_type(x, y, z):
    assert type(x) == int or type(x) == float
    assert type(y) == int or type(y) == float
    assert type(z) == list

def a2_2_type():
    with Preamble('from tests.cases.test_a2_2 import pollax_type as pollax'):
        func()


def pollax_reasonable(x, y, z):
    assert min(z) <= y < max(z)

def a2_2_reasonable():
    with Preamble('from tests.cases.test_a2_2 import pollax_reasonable as pollax'):
        func()

def run():
    return pytest.main(['tests/cases/test_a2_2.py'])