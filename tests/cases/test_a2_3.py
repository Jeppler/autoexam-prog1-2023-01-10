"""
Copyright (c) Jesper Carlsson 2023
"""

from tests.candidate_wrapper_func import func
import pytest
from pytest import MonkeyPatch, CaptureFixture

def test_a2_3_case1(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    monkeypatch.setattr('builtins.input', lambda _: 'ABC32 PX4 mNP 2F8')

    func()

    out, _ = capfd.readouterr()
    answer = out.split('\n')[0]

    assert answer[:8].lower() == 'resultat'
    assert answer[-5:]== ' APm2'

def test_a2_3_case2(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    monkeypatch.setattr('builtins.input', lambda _: '32 pollax Uppsala')

    func()

    out, _ = capfd.readouterr()
    answer = out.split('\n')[0]

    assert  answer[:8].lower() == 'resultat'
    assert  answer[-3:]== '3pU'

def test_a2_3_case3(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    monkeypatch.setattr('builtins.input', lambda _: 'Utan placerade palmer ska alla lyfta annanas')

    func()

    out, _ = capfd.readouterr()
    answer = out.split('\n')[0]

    assert  answer[:8].lower() == 'resultat'
    assert  answer[-7:]== 'Uppsala'

def test_a2_3_input(monkeypatch: MonkeyPatch):
    def input_test(my_str):
        assert len(my_str) > 1

        return ''

    monkeypatch.setattr('builtins.input', input_test)

    func()
    

def run():
    return pytest.main(['tests/cases/test_a2_3.py'])