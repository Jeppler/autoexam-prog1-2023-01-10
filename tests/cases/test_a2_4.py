"""
Copyright (c) Jesper Carlsson 2023
"""

from tests.candidate_wrapper_func import func
from tests.preable_controller import Preamble
import pytest
from pytest import CaptureFixture
from tests.redact import redact_varible

def test_a2_4_case1(capfd: CaptureFixture):
    with Preamble("a = { 'bok' : 4, 'ek' : 2, 'alm' : 1, 'en' : 3, 'asp' : 4 }"):
        func()

        out, _ = capfd.readouterr()
        rows = out.split('\n')

        assert rows[0] == "['bok', 'ek', 'alm', 'en', 'asp']"
        assert rows[1] == "[4, 2, 1, 3, 4]"

def test_a2_4_case2(capfd: CaptureFixture):
    with Preamble("a = { 'lönn' : 4, 'ek' : 0, 'al' : 1 }"):
        func()

        out, _ = capfd.readouterr()
        rows = out.split('\n')

        assert rows[0] == "['lönn', 'ek', 'al']"
        assert rows[1] == "[4, 0, 1]"

def test_a2_4_case3(capfd: CaptureFixture):
    with Preamble("a = {'tupp': 10, 'anka': 18}"):
        func()

        out, _ = capfd.readouterr()
        rows = out.split('\n')

        assert rows[0] == "['tupp', 'anka']"
        assert rows[1] == "[10, 18]"
    

def run():
    redact_varible('a')
    return pytest.main(['tests/cases/test_a2_4.py'])