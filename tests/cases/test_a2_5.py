"""
Copyright (c) Jesper Carlsson 2023
"""

from tests.candidate_wrapper_func import func
from tests.preable_controller import Preamble
import pytest
from pytest import CaptureFixture
from tests.redact import redact_varible

def test_a2_5_case1(capfd: CaptureFixture):
    with Preamble('x = [2, 6, 3, 7]; y = [5, 14, 2, 8]'):
        func()

        out, _ = capfd.readouterr()
        rows = out.split('\n')

        assert rows[0] == "[(2, 5), (6, 14), (3, 2), (7, 8)]"

def test_a2_5_case2(capfd: CaptureFixture):
    with Preamble('x = [1, 3]; y = [2, 4]'):
        func()

        out, _ = capfd.readouterr()
        rows = out.split('\n')

        assert rows[0] == "[(1, 2), (3, 4)]"
    

def run():
    redact_varible('x', 'y')
    return pytest.main(['tests/cases/test_a2_5.py'])