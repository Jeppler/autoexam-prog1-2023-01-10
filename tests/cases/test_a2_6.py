"""
Copyright (c) Jesper Carlsson 2023
"""

import tests.candidate_wrapper_class
import pytest
import importlib

def test_a2_6_case1():
    assert tests.candidate_wrapper_class.module.addera('10 15 7 4') == 36

def test_a2_6_case2():
    assert tests.candidate_wrapper_class.module.addera('6') == 6

def test_a2_6_case3():
    assert tests.candidate_wrapper_class.module.addera('3 -10 2') == -5

def test_a2_6_case4():
    assert tests.candidate_wrapper_class.module.addera('50 -8') == 42
    
def run():
    importlib.reload(tests.candidate_wrapper_class)
    return pytest.main(['tests/cases/test_a2_6.py'])