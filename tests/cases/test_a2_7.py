"""
Copyright (c) Jesper Carlsson 2023
"""

from tests.candidate_wrapper_func import func
import pytest
from pytest import MonkeyPatch, CaptureFixture

def test_a2_7_runnable():
    func()

def test_a2_7_rand_args(monkeypatch: MonkeyPatch):
    values = [1, 2, 3]

    def rand(lower: int, higher: int):
        import random

        assert lower == 1
        assert higher == 3

        return values.pop(0)

    monkeypatch.setattr('random.randint', rand)

    func()

def test_a2_7_case1(monkeypatch: MonkeyPatch, capfd: CaptureFixture):
    values = [3, 1, 1, 3, 1, 2]

    monkeypatch.setattr('random.randint', lambda _l, _h: values.pop(0))

    func()

    out, _ = capfd.readouterr()
    
    assert out == 'Kast med tresidig tärning\n' + \
        'Kast 1 tärningen visar 3\n' + \
        'Kast 2 tärningen visar 1\n' + \
        'Kast 3 tärningen visar 1\n' + \
        'Kast 4 tärningen visar 3\n' + \
        'Kast 5 tärningen visar 1\n' + \
        'Kast 6 tärningen visar 2\n' + \
        'Tärningens alla sidor har nu förekommit\n'
    

def run():
    return pytest.main(['tests/cases/test_a2_7.py'])