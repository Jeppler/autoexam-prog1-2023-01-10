"""
Copyright (c) Jesper Carlsson 2023
"""

import tests.candidate_wrapper_class
import pytest
import importlib

def test_b3_1_case1():
    assert tests.candidate_wrapper_class.module.toDict('ca ab Ca bF bg CH') == {'C': ['ca', 'Ca', 'CH'], 'A': ['ab'], 'B': ['bF', 'bg']}

def test_b3_1_case2():
    assert tests.candidate_wrapper_class.module.toDict('bla map Abba fd abcRst ab fk') == {'B': ['bla'], 'M': ['map'], 'A': ['Abba', 'abcRst', 'ab'], 'F': ['fd', 'fk']}

def test_b3_1_case3():
    assert tests.candidate_wrapper_class.module.toDict('I tidernas begynnelse fanns bara Tuppen') == {'I': ['I'], 'T': ['tidernas', 'Tuppen'], 'B': ['begynnelse', 'bara'], 'F': ['fanns']}

def run():
    importlib.reload(tests.candidate_wrapper_class)
    return pytest.main(['tests/cases/test_b1.py'])