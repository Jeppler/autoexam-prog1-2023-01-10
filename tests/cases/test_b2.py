"""
Copyright (c) Jesper Carlsson 2023
"""

import tests.candidate_wrapper_class
import importlib
import pytest

def test_b3_2_case1():
    q = tests.candidate_wrapper_class.module.QuPr()
    assert str(q) == '[]'
    for x in [3, 1, 3, 4, 1]:
        q.enter(x)
            
    assert str(q) == '[3, 1, 3, 4, 1]'

    assert not q.is_empty()
    assert q.retrive() == 1
    assert str(q) == '[3, 3, 4, 1]'

    assert not q.is_empty()
    assert q.retrive() == 1
    assert str(q) == '[3, 3, 4]'

    assert not q.is_empty()
    assert q.retrive() == 3
    assert str(q) == '[3, 4]'

    assert not q.is_empty()
    assert q.retrive() == 3
    assert str(q) == '[4]'

    assert not q.is_empty()
    assert q.retrive() == 4
    assert str(q) == '[]'

    assert q.is_empty()

def test_b3_2_case2():
    q = tests.candidate_wrapper_class.module.QuPr()

    [q.enter(x) for x in [2, 5, 3, 1]]

    assert not q.is_empty()
    assert q.retrive() == 1
    assert q.retrive() == 2
    assert q.retrive() == 3
    assert q.retrive() == 5
    assert q.is_empty()

def run():
    importlib.reload(tests.candidate_wrapper_class)
    return pytest.main(['tests/cases/test_b2.py'])