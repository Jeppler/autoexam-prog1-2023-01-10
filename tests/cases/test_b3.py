"""
Copyright (c) Jesper Carlsson 2023
"""

from tests.candidate_wrapper_func import func
from tests.preable_controller import Preamble
from tests.redact import redact_pattern
import pytest
from pytest import CaptureFixture

def koeff2Str(koeff):
    def _koeff2Str(koeff, n=0):
        if len(koeff) == 0:
            return []
        
        return ([] if koeff[0] == 0 else [f'{koeff[0]}x^{n}']) + _koeff2Str(koeff[1:], n+1)
    return ' + '.join(_koeff2Str(koeff))

def test_b3_3_case1(capfd: CaptureFixture):
    with Preamble('from tests.cases.test_b3_3 import koeff2Str\nkoeff = [2, 4, 0, 5]'):
        func()

        out, _ = capfd.readouterr()

        rows = out.split('\n')

        assert rows[0] == 'Polynomet från början: [2, 4, 0, 5]'
        assert rows[1] == 'Representerat som en sträng: 2x^0 + 4x^1 + 5x^3'
        assert rows[2] == 'Nu skall polynomet deriveras...'
        assert rows[3] == 'Derivata 1: 4x^0 + 15x^2'
        assert rows[4] == 'Derivata 2: 30x^1'
        assert rows[5] == 'Derivata 3: 30x^0'
        assert rows[6] == 'Programmet avslutas'

def test_b3_3_case1(capfd: CaptureFixture):
    with Preamble('from tests.cases.test_b3_3 import koeff2Str\nkoeff = [1, 2, 3, 4, 5]'):
        func()

        out, _ = capfd.readouterr()

        rows = out.split('\n')

        assert rows[0] == 'Polynomet från början: [1, 2, 3, 4, 5]'
        assert rows[1] == 'Representerat som en sträng: 1x^0 + 2x^1 + 3x^2 + 4x^3 + 5x^4'
        assert rows[2] == 'Nu skall polynomet deriveras...'
        assert rows[3] == 'Derivata 1: 2x^0 + 6x^1 + 12x^2 + 20x^3'
        assert rows[4] == 'Derivata 2: 6x^0 + 24x^1 + 60x^2'
        assert rows[5] == 'Derivata 3: 24x^0 + 120x^1'
        assert rows[6] == 'Derivata 4: 120x^0'
        assert rows[7] == 'Programmet avslutas'


def run():
    redact_pattern('koeff\s*=\s*[.*].*')
    return pytest.main(['tests/cases/test_b3.py']) 