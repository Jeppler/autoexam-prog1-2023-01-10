"""
Copyright (c) Jesper Carlsson 2023
"""

import importlib
from pytest import ExitCode

def start_test(test_name: str, candidate_data: str) -> dict:
    """
    Writes ``candidate_data`` to file and runs test ``test_name``. 

    ## Parameters
     - ``test_name`` name of test-moodule in ``tests/`` folder. 
     - ``candidate_data`` python code to test. 

    ## Returns
     - Dictionary with boolans ``tested`` and ``correct``. 
    """
    
    # Write candidate data to file
    with open('tests/cases/candidate.py', 'wb') as file:
        file.write(bytes('import tests.preamble\nimport importlib\nimportlib.reload(tests.preamble)\nfrom tests.preamble import *\n\n', 'utf-8'))
        file.write(bytes(candidate_data, 'utf-8'))
    
    # Run test
    tested, correct = [False] * 2
        
    try: 
        tests = importlib.import_module(f'tests.cases.{test_name}')
        correct = tests.run() == ExitCode.OK
        tested = True
    except Exception as e:
        print(e.with_traceback())

    # Clear candidate file
    with open('tests/cases/candidate.py', 'w') as file:
        file.write('')
    
    # Return result
    return {
        'tested': tested,
        'correct': correct
    }