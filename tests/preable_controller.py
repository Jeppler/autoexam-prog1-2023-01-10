"""
Copyright (c) Jesper Carlsson 2023
"""

class Preamble:
    FILE_NAME = 'tests/preamble.py'

    def __init__(self, data):
        self.data = data

    def __enter__(self):
        with open(Preamble.FILE_NAME, 'wb') as file:
            file.write(bytes(self.data, 'utf-8'))

    def __exit__(self, *execptions):
        with open(Preamble.FILE_NAME, 'w') as file:
            file.write('')