"""
Copyright (c) Jesper Carlsson 2023
"""

import re

def redact_varible(*varibles: str) -> None:
    """
    Removes all variables in ``*varibles`` from candidate file. 
    """

    with open('tests/cases/candidate.py', 'r', encoding='utf-8') as file:
        document = re.sub(f'({"|".join(varibles)})\\s*=[^=].*', '', file.read())
    
    with open('tests/cases/candidate.py', 'w', encoding='utf-8') as file:
        file.write(document)

def redact_pattern(pattern: str) -> None:
    """
    Removes all variables in ``*varibles`` from candidate file. 
    """

    with open('tests/cases/candidate.py', 'r', encoding='utf-8') as file:
        document = re.sub(pattern, '', file.read())
    
    with open('tests/cases/candidate.py', 'w', encoding='utf-8') as file:
        file.write(document)
