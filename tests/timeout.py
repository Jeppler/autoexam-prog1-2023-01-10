"""
Copyright (c) Jesper Carlsson 2023
"""

from threading import Thread

def timeout(time_limit: float):
    """
    Decorator function that enforeces a time limit on a function. 

    ## Parameters
     - ``time_limit`` maxium time before timeout in seconds. 

    ## Raises
     - Raises ``TimeoutError`` if function times out. 
    """

    def decorator(func):
        def wrapper(*args):
            class Result:
                def __init__(self):
                    self.data = None
                    self.exception = None
            
            def _func(result: Result, *args):
                try:
                    result.data = func(*args)
                except Exception as e:
                    result.exception = e

            result = Result()

            thread = Thread(target=_func, args=[result] + list(args), daemon=True)
            thread.start()
            thread.join(time_limit)

            if thread.is_alive():
                raise TimeoutError
            
            if result.exception is not None:
                raise result.exception

            return result.data
        return wrapper
    return decorator
